package com.example.editcamera

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.editcamera.MainActivity
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import pl.aprilapps.easyphotopicker.EasyImage.ImageSource
import java.io.File

class MainActivity : AppCompatActivity() {
    @BindView(R.id.ivAvatar)
    var ivAvatar: ImageView? = null

    @BindView(R.id.btnChangePhoto)
    var btnChangePhoto: Button? = null
    var unbinder: Unbinder? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        unbinder = ButterKnife.bind(this)
        btnChangePhoto!!.setOnClickListener { /*
                Fungsi untuk memanggil library choose image
                 */
            EasyImage.openChooserWithGallery(this@MainActivity, "Choose Picture",
                    REQUEST_CHOOSE_IMAGE)
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        // method untuk menghandle ketika user sudah memilih gambar.
        // ketika gambar sudah dipilih maka gambar akan di redirect ke activity
        // library android-image-picker
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, object : DefaultCallback() {
            override fun onImagePicked(imageFile: File, source: ImageSource, type: Int) {
                CropImage.activity(Uri.fromFile(imageFile))
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .setFixAspectRatio(true)
                        .start(this@MainActivity)
            }

            override fun onImagePickerError(e: Exception, source: ImageSource, type: Int) {
                super.onImagePickerError(e, source, type)
                Toast.makeText(this@MainActivity, e.message, Toast.LENGTH_SHORT).show()
            }

            override fun onCanceled(source: ImageSource, type: Int) {
                super.onCanceled(source, type)
            }
        })
        // ----

        // Method ini berfungsi ketika sudah selesai dari activity android-image-picker
        // Jika result_ok maka gambar yang sudah di crop akan dimasukan kedalam imageview
        // yang kita olah menggunakan library glide.
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                val resultUri = result.uri
                Glide.with(this)
                        .load(File(resultUri.path))
                        .apply(RequestOptions().circleCrop())
                        .into(ivAvatar!!)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unbinder!!.unbind()
    }

    companion object {
        private const val REQUEST_CHOOSE_IMAGE = 3
    }
}